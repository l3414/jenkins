package ru.lanit.at.steps.web;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.Если;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import io.restassured.http.ContentType;
import ru.lanit.at.actions.WebActions;
import ru.lanit.at.utils.Sleep;
import ru.lanit.at.utils.web.pagecontext.PageManager;

import java.io.File;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static io.restassured.RestAssured.given;
import static ru.lanit.at.utils.VariableUtil.replaceVars;


public class WebActionWebSteps extends AbstractWebSteps {

    public WebActionWebSteps(PageManager pageManager) {
        super(pageManager);
    }


    /**
     * нажимает на элемент по тексту
     *
     * @param text текст элемента
     */
    @Когда("кликнуть на элемент по тексту {string}")
    public void clickElementWithText(String text) {
        $(Selectors.byText(text))
                .shouldBe(Condition.visible)
                .click();
        LOGGER.info("клик на элемент по тексту '{}'", text);
    }

    @Если("кликнуть на элемент {string}")
    public void clickOnElement(String elementName) {
        SelenideElement element = pageManager
                .getCurrentPage()
                .getElement(elementName);
        element
                .shouldBe(Condition.visible, Duration.ofSeconds(10))
                .click();
        LOGGER.info("клик на элемент '{}'", elementName);
    }

    @Если("установить чекбокс на элементе {string}")
    public void selectCheckboxOnElement(String elementName) {
        SelenideElement element = pageManager
                .getCurrentPage()
                .getElement(elementName);
        element
                .shouldBe(Condition.enabled, Duration.ofSeconds(60));
        WebActions.setCheckBox(element, true);
        LOGGER.info("чекбокс  установлен на элементе '{}'", elementName);
    }

    @Если("убрать чекбокс на элементе {string}")
    public void unselectCheckboxOnElement(String elementName) {
        SelenideElement element = pageManager
                .getCurrentPage()
                .getElement(elementName);
        element
                .shouldBe(Condition.enabled, Duration.ofSeconds(60));
        WebActions.setCheckBox(element, false);
        LOGGER.info("чекбокс снят на элементе '{}'", elementName);
    }


    /**
     * скролл до элемента
     *
     * @param elementName наименование элемента
     */
    @Когда("проскроллить страницу до элемента {string}")
    public void scrollToElement(String elementName) {
        SelenideElement element = pageManager.getCurrentPage().getElement(elementName);
        element.shouldBe(Condition.visible)
                .scrollIntoView("{block: 'center'}");
        LOGGER.info("скролл страницы до элемента '{}'", elementName);
    }

    /**
     * скролл до текста
     *
     * @param text текст
     */
    @Когда("проскроллить страницу до текста {string}")
    public void scrollToText(String text) {
        SelenideElement element = $(Selectors.byText(text));
        element.shouldBe(Condition.visible)
                .scrollIntoView("{block: 'center'}");
        LOGGER.info("скролл страницы до текста '{}'", text);
    }

    @И("подождать {int} сек")
    public void waitSeconds(int timeout) {
        Sleep.pauseSec(timeout);
    }

    /**
     * Ввод значения в элемент
     *
     * @param field - наименование элемента
     * @param value - значение
     */
    @Когда("ввести в поле {string} значение {string}")
    public void fillTheField(String field, String value) {
        value = replaceVars(value, getStorage());
        SelenideElement fieldElement = pageManager
                .getCurrentPage()
                .getElement(field);
        fieldElement
                .shouldBe(Condition.visible, Duration.ofSeconds(60))
                .setValue(value);
        LOGGER.info("в поле '{}' введено значение '{}'", field, value);
    }

    /**
     * Сохранить  значения из элемент
     *
     * @param field - наименование элемента
     * @param key   - значение
     */
    @Когда("сохранить значение из поля {string} под именем {string}")
    public void saveValueField(String field, String key) {
        SelenideElement fieldElement = pageManager
                .getCurrentPage()
                .getElement(field);
        String elementValue = fieldElement
                .shouldBe(Condition.visible, Duration.ofSeconds(60))
                .getValue();
        saveValueInStorage(key, elementValue);
        LOGGER.info("значение '{}' сохранено под именем '{}'", elementValue, key);
    }

    @Когда("сохранить текст из поля {string} под именем {string}")
    public void saveTextField(String field, String key) {
        SelenideElement fieldElement = pageManager
                .getCurrentPage()
                .getElement(field);
        String elementValue = fieldElement
                .shouldBe(Condition.visible, Duration.ofSeconds(60))
                .getText();
        saveValueInStorage(key, elementValue);
        LOGGER.info("значение '{}' сохранено под именем '{}'", elementValue, key);
    }

    @Когда("Получить данные по ключу {string}")
    public String getValueByKey(String key) {
        String value = String.valueOf(getStorage().get(key));
        LOGGER.info("значение '{}' получено под ключом '{}'", value, key);
        return value;
    }


    /**
     * Очистка поля
     *
     * @param elementName наименование элемента
     */
    @Если("очистить поле {string}")
    public void clearFiled(String elementName) {
        pageManager
                .getCurrentPage()
                .getElement(elementName)
                .shouldBe(Condition.visible)
                .clear();
    }

    @Когда("выбрать в селекте {string} значение {string}")
    public void fillTheSelect(String select, String value) {
        value = replaceVars(value, getStorage());
        SelenideElement selectField = pageManager
                .getCurrentPage()
                .getElement(select);
        selectField.selectOption(value);
        LOGGER.info("в селекте '{}' введено значение '{}'", select, value);
    }

    /**
     * Выбор случайного значения из выпадающего списка
     *
     * @param select наименование элемента ссылки на выпадающий список
     * @param list список значений выпадающегшо списка на выпадающий список
     */
    @Когда("выбрать в селекте с названием {string} случайное значение из выпадающего списка {string}")
    public void fillTheSelectRandom(String select, String list) {
        List<String> listElem = pageManager.getCurrentPage().getElementsCollection(list).texts();
        int rand = (int) (Math.random()*listElem.size());
        String value = listElem.get(rand);

        value = replaceVars(value, getStorage());
        SelenideElement selectField = pageManager
                .getCurrentPage()
                .getElement(select);
        selectField.selectOption(value);
        LOGGER.info("в селекте '{}' введено значение '{}'", select, value);
    }


    @Когда("выбрать в селекте случайное значение из выпадающего списка {string}")
    public void fillTheSelectRandomSingle(String select) {
        List<String> listElem = pageManager.getCurrentPage().getElement(select).$$("option").texts();
        int rand = (int) (Math.random()*listElem.size());


        String value = listElem.get(rand);

        value = replaceVars(value, getStorage());
        SelenideElement selectField = pageManager
                .getCurrentPage()
                .getElement(select);
        selectField.selectOption(value);
        LOGGER.info("в селекте '{}' введено значение '{}'", select, value);

    }


    /**
     * Загрузка файла
     *
     * @param elementName наименование элемента
     * @param path        путь до загружаемого файла
     */

    @Когда("загрузить файл, расположенный в {string} в поле {string}")
    public void uploadFile(String elementName, String path) {
        SelenideElement fieldElement = pageManager
                .getCurrentPage()
                .getElement(elementName);
        fieldElement
                .uploadFile(new File(path));
        LOGGER.info("в поле '{}' загружен файл, расположенный в '{}' ", elementName, path);
    }

    /**
     * Получение Отп токена для авторизации
     *
     * @param webElement наименование элемента
     * @param login      логин пользователя
     * @param password      логин пользователя
     */
    @Тогда("ввести токен в поле {string} для пользователя {string} с паролем {string}")
    public void insertToken(String webElement, String login, String password){
        String json = "{\"username\":\"" + login + "\",\n\"password\":\"" + password + "\"}";
        String token= given()
                    .baseUri("http://178.154.246.238:58082/")
                    .contentType(ContentType.JSON)
                    .accept(ContentType.JSON)
                    .body(json)
                    .when()
                    .post("/api/otp_token/")
                    .then()
                    .extract().body().jsonPath().getString("otp_token");
        token = replaceVars(token, getStorage());
        SelenideElement fieldElement = pageManager
                  .getCurrentPage()
                  .getElement(webElement);
        fieldElement
                  .shouldBe(Condition.visible, Duration.ofSeconds(60))
                  .setValue(token);


    }



}

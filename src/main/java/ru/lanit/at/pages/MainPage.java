package ru.lanit.at.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.lanit.at.utils.web.annotations.Name;
import ru.lanit.at.utils.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "Главная страница")
public class MainPage extends WebPage {

    @Name("ссылка сотрудники")
    private SelenideElement linkEmployees = $x("//tr[@class='model-employee']/th[@scope='row']/a");

    @Name("ссылка Администраторы аккаунта")
    private SelenideElement linkAccountAdmin = $x("//tr[@class='model-accountadmin']/th[@scope='row']/a");

    @Step("Нажать на cсылку \"сотрудники\"")
    public void clickOnLinkEmployees() {
        linkEmployees.click();
    }

    @Step("Нажать на cсылку \"Администраторы аккаунта\"")
    public void clickOnLinkAccountadmin() {
        linkAccountAdmin.click();
    }


}

package api.cucumber_style;

import api.testng_style.fragments.Sities;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import io.cucumber.datatable.DataTable;
import io.cucumber.datatable.DataTableTypeRegistry;
import io.cucumber.datatable.DataTableTypeRegistryTableConverter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.lanit.at.api.models.RequestModel;
import ru.lanit.at.steps.VariableSteps;
import ru.lanit.at.steps.api.ApiSteps;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class CitiesTest {
    private final ApiSteps apiSteps = new ApiSteps();
    private final VariableSteps variableSteps = new VariableSteps();

    @BeforeMethod
    public void authorization() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config/configuration.properties"));
        RequestModel requestModel = new RequestModel("POST", "authAdmin.json", null, System.getProperty("baseUrl")+"/login");
        apiSteps.createRequest(requestModel);
        DataTable dataTable = DataTable.create(
                Arrays.asList(
                        Arrays.asList("Content-Type", "application/json"),
                        Arrays.asList("Accept", "application/json")
                ), new DataTableTypeRegistryTableConverter(new DataTableTypeRegistry(Locale.ENGLISH)));
        apiSteps.addHeaders(dataTable);
        apiSteps.send();
        apiSteps.expectStatusCode(200);
        Map<String, String> map = new HashMap<>();
        map.put("token", "$.token");
        apiSteps.extractVariables(map);
    }

    @Test
    public void getCitiesTest() throws IOException, ProcessingException {
        RequestModel requestModel = new RequestModel("GET", null, null, System.getProperty("baseUrl")+"/cities/");
        apiSteps.createRequest(requestModel);
        DataTable dataTable = DataTable.create(Arrays.asList(
                Arrays.asList("Content-Type", "application/json"),
                Arrays.asList("Accept", "application/json")
        ), new DataTableTypeRegistryTableConverter(new DataTableTypeRegistry(Locale.ENGLISH)));
        apiSteps.addHeaders(dataTable);
        apiSteps.addAuthorisation();
        apiSteps.send();
        apiSteps.expectStatusCode(200);
        apiSteps.checkShema("src/test/resources/jsonShema/GetSities.json");
    }


    @Test
    public void postCitiesTest() throws IOException, ProcessingException {
        String randomSity = Sities.getRandomSity();
        Map<String, String> newSity = new HashMap<>();
        newSity.put("name", randomSity);
        variableSteps.generateVariablesSity(newSity);

        RequestModel requestModel = new RequestModel("POST", "createNewSity.json", null, System.getProperty("baseUrl")+"/cities/");
        apiSteps.createRequest(requestModel);
        DataTable dataTable = DataTable.create(Arrays.asList(
                Arrays.asList("Content-Type", "application/json"),
                Arrays.asList("Accept", "application/json")
        ), new DataTableTypeRegistryTableConverter(new DataTableTypeRegistry(Locale.ENGLISH)));
        apiSteps.addHeaders(dataTable);
        apiSteps.addAuthorisation();
        apiSteps.send();
        apiSteps.expectStatusCode(201);
        apiSteps.checkShema("src/test/resources/jsonShema/PostSity.json");

    }


    }







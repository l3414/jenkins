package api.testng_style;

import api.testng_style.pogoFiles.EmployeesPOGO;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.lanit.at.dictionaries.PathOnLogin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class AdministratorsTest {
    private String token;

    private String generateJson(PathOnLogin login) {
        return "{\"username\":\"" + login.getLogin() + "\",\n\"password\":\"" + login.getPassword() + "\"}";

    }

    @BeforeClass
    public void prepare() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config/configuration.properties"));
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(System.getProperty("site.url"))
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
        RestAssured.filters(new ResponseLoggingFilter());
    }

    @BeforeMethod
    public void authorization(){
        this.token = given()
                .body(generateJson(PathOnLogin.ADMIN))
                .when()
                .post("/api/login/")
                .then()
                .statusCode(200)
                .extract().body().jsonPath().getString("token");
    }


    @Test
    public void getAdministratorTest() {
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/administrators/")
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/GetAdministrators.json")));
    }


    @Test
    public void postAdministratorTest(){
        List<Integer> administrators = new ArrayList<>();
        List<EmployeesPOGO> emplooyers = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/employees/")
                .then()
                .extract().body().jsonPath().getList("$",EmployeesPOGO.class);
        for(EmployeesPOGO i: emplooyers) {
            if (i.getCurrent_positions().contains("АДМ - Администратор проекта"))
                administrators.add(i.getId());
        }
        int randId = administrators.get((int) (Math.random()*administrators.size()));
            String res = String.format("{\"employee\": %d}",randId);

        given()
                .header(new Header("Authorization", "Token " + token))
                .body(res)
                .when()
                .post("/api/administrators/")
                .then()
                .log().all()
                .statusCode(201);
    }

    @Test
    public void getAdministratorByIdTest(){
        List<Integer> idAdmins = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/administrators/")
                .then()
                .extract().body().jsonPath().getList("id");
        int randId = idAdmins.get((int) (Math.random()*idAdmins.size()));
        String res = String.format("/api/administrators/"+randId);


        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get(res)
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/GetAdministratorsById.json")));
    }

    @Test
    public void patchAdministratorByIdTest(){
        List<Integer> idAdmins = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/administrators/")
                .then()
                .extract().body().jsonPath().getList("id");
        int randId = idAdmins.get(idAdmins.get((int) (Math.random()*idAdmins.size())));
        String res = String.format("/api/administrators/"+randId);
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .body("{\"employee\":881}")
                .patch(res)
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/PatchAdministratorById.json")));

    }

    @Test
    public void deleteAdministratorByIdTest(){
        List<Integer> idAdmins = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/administrators/")
                .then()
                .extract().body().jsonPath().getList("id");
        int randId = idAdmins.get(idAdmins.get((int) (Math.random()*idAdmins.size())));
        String res = String.format("/api/administrators/"+randId);
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .delete(res)
                .then()
                .log().all();
    }










}


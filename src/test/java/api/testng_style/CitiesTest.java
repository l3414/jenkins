package api.testng_style;

import api.testng_style.fragments.Sities;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.lanit.at.dictionaries.PathOnLogin;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static io.restassured.RestAssured.given;

public class CitiesTest {
    private String token;

    private String generateJson(PathOnLogin login) {
        return "{\"username\":\"" + login.getLogin() + "\",\n\"password\":\"" + login.getPassword() + "\"}";

    }

    @BeforeClass
    public void prepare() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config/configuration.properties"));
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(System.getProperty("site.url"))
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
        RestAssured.filters(new ResponseLoggingFilter());
    }

    @BeforeMethod
    public void authorization(){
        this.token = given()
                .body(generateJson(PathOnLogin.ADMIN))
                .when()
                .post("/api/login/")
                .then()
                .statusCode(200)
                .extract().body().jsonPath().getString("token");
    }


    @Test
    public void getCitiesTest() {
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/")
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/GetSities.json")));
    }


    @Test
    public void postCitiesTest(){
        String randomSity = Sities.getRandomSity();
        String res = String.format("{\"name\":\"%s\"}",randomSity);

        int idPost = given()
                .header(new Header("Authorization", "Token " + token))
                .body(res)
                .when()
                .post("/api/cities/")
                .then()
                .log().all()
                .statusCode(201)
                .extract().body().jsonPath().get("id");

        String resCheck = "/api/cities/"+idPost;
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get(resCheck)
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/PostSity.json")));
    }

    @Test
    public void getCitiesByIdTest(){
        List<Integer> idSities = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/")
                .then()
                .extract().body().jsonPath().getList("id");
        int randId = idSities.get((int) (Math.random()*idSities.size()));
        String res = "/api/cities/"+randId;


        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get(res)
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/PostSity.json")));
    }

    @Test
    public void patchCitiesByIdTest(){

        String randomSity = Sities.getRandomSity();
        String body = String.format("{\"name\":\"%s\"}",randomSity);

        List<Integer> idSities = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/")
                .then()
                .extract().body().jsonPath().getList("id");

        int randId = idSities.get(((int) (Math.random()*idSities.size())));
        String res = "/api/cities/"+randId;

        RestAssured.given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .body(body)
                .patch(res)
                .then()
                .statusCode(200);

        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/"+randId)
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/jsonShema/PostSity.json")));

    }

    @Test
    public void deleteSityByIdTest(){
        List<Integer> idSities = given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/")
                .then()
                .extract().body().jsonPath().getList("id");
        int randId = idSities.get(((int) (Math.random()*idSities.size())));
        String res = String.format("/api/cities/"+randId);
        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .delete(res);

        given()
                .header(new Header("Authorization", "Token " + token))
                .when()
                .get("/api/cities/"+randId)
                .then()
                .statusCode(404);


    }










}

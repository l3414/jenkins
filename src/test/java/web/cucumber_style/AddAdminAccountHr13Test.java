package web.cucumber_style;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.lanit.at.steps.web.DebugWebSteps;
import ru.lanit.at.steps.web.WebActionWebSteps;
import ru.lanit.at.steps.web.WebCheckWebSteps;
import ru.lanit.at.steps.web.WindowWebSteps;
import ru.lanit.at.utils.web.pagecontext.PageManager;
import web.MainTest;
import web.PathOnLogin;



import java.io.IOException;


public class AddAdminAccountHr13Test extends MainTest {

    @DataProvider
    public Object[][] dataAddAdminAccountTest1() {
        return new Object[][]{
                {"01.01.2021","Тестирование превыше всего", "was added successfully" }
        };
    }

    /* Для осуществления поиска элементов в контексте конкртеной страницы */
    private final PageManager pageManager = new PageManager();

    /* Классы шагов */
    private final DebugWebSteps debugWebSteps = new DebugWebSteps(pageManager);
    private final WindowWebSteps windowWebSteps = new WindowWebSteps(pageManager);
    private final WebActionWebSteps webActionWebSteps = new WebActionWebSteps(pageManager);
    private final WebCheckWebSteps webCheckWebSteps = new WebCheckWebSteps(pageManager);

    @BeforeMethod
    public void beforeTest() throws IOException {
        PathOnLogin user = PathOnLogin.hr;
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("accounts.properties"));

        windowWebSteps.open(System.getProperty("site.url"));
        windowWebSteps.setPage("Страница авторизации");
        webActionWebSteps.fillTheField("Логин", user.getLogin());
        webActionWebSteps.fillTheField("Пароль", System.getProperty(user.getLogin()));
        webActionWebSteps.clickOnElement("Чекбокс \"Админские права\"");
        webCheckWebSteps.elementAppearOnThePage("Отп токен");
        webActionWebSteps.fillTheField("Отп токен",getAuthToken (user));
        System.out.println(getAuthToken (user));
        webActionWebSteps.clickOnElement("Кнопка \"Войти\"");
        windowWebSteps.setPage("Главная страница");
        webActionWebSteps.clickOnElement("ссылка Администраторы аккаунта");
        windowWebSteps.setPage("Администраторы аккаунта");
        webActionWebSteps.clickOnElement("ДОБАВИТЬ АДМИНИСТРАТОР АККАУНТА+");
    }

    @Test(dataProvider = "dataAddAdminAccountTest1")
    public void addAdminAccountTest1(String date,String note, String successMsg){
        step1_1();
        step1_2();
        step1_3(date);
        step1_4(note);
        step1_5();
        step2(successMsg);
    }


    private void step1_1(){
        debugWebSteps.stepNumber("1_1");
        windowWebSteps.setPage("Добавить администратора аккаунта");
        webActionWebSteps.fillTheSelectRandomSingle("Сотрудник");
    }


    private void step1_2(){
        debugWebSteps.stepNumber("1_2");
        webActionWebSteps.fillTheSelectRandomSingle("Аккаунт");
    }

    private void step1_3(String date){
        debugWebSteps.stepNumber("1_3");
        webActionWebSteps.fillTheField("Дата начала работы", date);
    }


    private void step1_4(String note){
        debugWebSteps.stepNumber("1_4");
        webActionWebSteps.fillTheField("Заметка", note);
    }

    private void step1_5(){
        debugWebSteps.stepNumber("1_5");
        webActionWebSteps.clickOnElement("Кнопка сохранить");
    }

    private void step2(String successMsg){
        debugWebSteps.stepNumber("2");
        windowWebSteps.setPage("Администраторы аккаунта");
        webActionWebSteps.saveTextField("Успешное добавление администратора", "msg");
        Assert.assertTrue(webActionWebSteps.getValueByKey("msg").contains(successMsg));

    }

}

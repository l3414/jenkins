package ru.lanit.at.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.lanit.at.utils.web.annotations.Name;
import ru.lanit.at.utils.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Name(value = "Добавить администратора аккаунта")
public class AddAccountAdminPage extends WebPage {

    @Name("Сотрудник")
    private SelenideElement nameEmployer =$x("//select[@name='employee']");

    @Name("Аккаунт")
    private SelenideElement account =$x("//select[@name=\"account_name\"]");

    @Name("Дата начала работы")
    private SelenideElement dateStartWork = $x("//input[@name='start_date']");

    @Name("Дата начала работы сегодня")
    private SelenideElement dateStartWorkToday = $x("//div[@class='form-row field-start_date']//a[1]");

    @Name("Заметка")
    private SelenideElement note = $x("//textarea[@name='memo']");

    @Name("Кнопка сохранить")
    private SelenideElement saveButton = $x("//input[@name='_save']");


    private int randomElement(int element){
        return ((int) (Math.random()*element));
    }


    @Step("заполнить поле \"Сотрудник\" cлучайным значением ")
    public AddAccountAdminPage setAddEmployeeRandom(){
        int rand = randomElement(nameEmployer.$$("option").size());
        nameEmployer.selectOption(rand);
        return this;
    }

    @Step("заполнить поле \"Сотрудник\" значением {text}")
    public AddAccountAdminPage setAddEmployee(String name){
        nameEmployer.selectOption(name);
        return this;
    }

    @Step("заполнить поле \"Аккаунт\" cлучайным значением ")
    public AddAccountAdminPage setAddAccountRandom(){
        int rand = randomElement(account.$$("option").size());
        account.selectOption(rand);
        return this;
    }

    @Step("заполнить поле \"Аккаунт\"  значением {text}")
    public AddAccountAdminPage setAddAccount(String accountName){
        account.selectOption(accountName);
        return this;
    }

    @Step("выбрать дату начала работы сегодня")
    public AddAccountAdminPage setStartWorkEmployerToday(){
        dateStartWorkToday.click();
        return this;
    }

    @Step("выбрать дату начала работы c числа{date}")
    public AddAccountAdminPage setStartWorkEmployerDate(String date){
        dateStartWork.sendKeys(date);
        return this;
    }

    @Step("нажать на кнопку \"Сохранить\" ")
    public void clickButtonSave() {
        saveButton.click();
    }

    @Step("заполнить поле \"Заметка\" значением {text} ")
    public AddAccountAdminPage setNote(String text){
        note.sendKeys(text);
        return this;
    }


}

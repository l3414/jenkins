package ru.lanit.at.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.lanit.at.utils.web.annotations.Name;
import ru.lanit.at.utils.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "Администраторы аккаунта")
public class AccountAdminPage extends WebPage {


    @Name("ДОБАВИТЬ АДМИНИСТРАТОР АККАУНТА+")
    private SelenideElement buttonAddNewAccountAdmin = $x("//a[@class='addlink']");

    @Name("Успешное добавление администратора")
    private SelenideElement linkSuccessAddAccountAdmin = $x("//*[text()[contains(.,'was added successfully')]]");

    @Step("Нажать на кнопку \"ДОБАВИТЬ АДМИНИСТРАТОР АККАУНТА+\"")
    public void clickOnButtonAddNewAccountAdmin(){
        buttonAddNewAccountAdmin.click();
    }

    @Step("Получить значение ссылки на успешное добавление администратора аккаунта")
    public String getResultString(){
        return linkSuccessAddAccountAdmin.getText();
    }

}

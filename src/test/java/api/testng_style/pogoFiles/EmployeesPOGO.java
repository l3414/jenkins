package api.testng_style.pogoFiles;

import java.util.ArrayList;

public class EmployeesPOGO {
    private String name;
    private String surname;
    private String patronymic;
    private ArrayList<String> current_positions;
    private String current_project;
    private Object current_city;
    private String joining_date;
    private Integer id;

    public EmployeesPOGO() {
    }

    public EmployeesPOGO(String name, String surname, String patronymic, ArrayList<String> current_positions, String current_project, Object current_city, String joining_date, int id) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.current_positions = current_positions;
        this.current_project = current_project;
        this.current_city = current_city;
        this.joining_date = joining_date;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public ArrayList<String> getCurrent_positions() {
        return current_positions;
    }

    public void setCurrent_positions(ArrayList<String> current_positions) {
        this.current_positions = current_positions;
    }

    public String getCurrent_project() {
        return current_project;
    }

    public void setCurrent_project(String current_project) {
        this.current_project = current_project;
    }

    public Object getCurrent_city() {
        return current_city;
    }

    public void setCurrent_city(Object current_city) {
        this.current_city = current_city;
    }

    public String getJoining_date() {
        return joining_date;
    }

    public void setJoining_date(String joining_date) {
        this.joining_date = joining_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

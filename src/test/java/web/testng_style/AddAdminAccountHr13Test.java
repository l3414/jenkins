package web.testng_style;

import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.lanit.at.pages.*;
import ru.lanit.at.steps.web.WindowWebSteps;
import web.MainTest;
import web.PathOnLogin;

import java.io.IOException;

public class AddAdminAccountHr13Test extends MainTest {

    @DataProvider
    public Object[][] dataAddAdminAccountTest1() {
        return new Object[][]{
                {"01.01.2021","Тестирование превыше всего", "was added successfully" }
        };
    }

    
    private final WindowWebSteps windowWebSteps = new WindowWebSteps(null);

    private final MainPage mainPage = new MainPage();
    private final AuthPage authPage = new AuthPage();
    private final AccountAdminPage accountAdminPage = new AccountAdminPage();
    private final AddAccountAdminPage addAccountAdminPage = new AddAccountAdminPage();

    @BeforeMethod
    public void beforeTest() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("accounts.properties"));
        PathOnLogin user = PathOnLogin.hr;
        windowWebSteps.open(System.getProperty("site.url"));
        authPage.fillFieldLogin(user.getLogin())
                .fillFieldPassword(System.getProperty(user.getLogin()))
                .clickOnCheckBoxAdminRules()
                .searchOtpTokenShouldBeVisible()
                .fillFieldOtpToken(getAuthToken(user))
                .clickOnButtonSignIn();
        mainPage.clickOnLinkAccountadmin();
    }

    @Test(dataProvider = "dataAddAdminAccountTest1")
    public void addAdminAccountTest1(String date,String note, String successMsg){
        step1_1();
        step1_2();
        step1_3();
        step1_4(date);
        step1_5(note);
        step1_6();
        step2(successMsg);
        }


    @Step("Шаг №1")
    private void step1_1(){
        accountAdminPage.clickOnButtonAddNewAccountAdmin();
    }

    @Step("Шаг №2")
    private void step1_2(){
        addAccountAdminPage.setAddEmployeeRandom();
    }

    @Step("Шаг №3")
    private void step1_3(){
        addAccountAdminPage.setAddAccountRandom();
    }

    @Step("Шаг 4")
    private void step1_4(String date){
        addAccountAdminPage.setStartWorkEmployerDate(date);
    }

    @Step("Шаг №5")
    private void step1_5(String note){
        addAccountAdminPage.setNote(note);
    }

    @Step("Шаг №6")
    private void step1_6(){
        addAccountAdminPage.clickButtonSave();
    }

    @Step("Шаг №7")
    private void step2(String successMsg){
        Assert.assertTrue(accountAdminPage.getResultString().contains(successMsg));
    }

}
